//
//  battleship.hpp
//  
//
//  Created by Lucas Chavarria on 2/20/17.
//
//

#ifndef battleship_hpp
#define battleship_hpp

#include <iostream>
#include <cstdlib>

void initboard(int ***board);
void insertShipV(int col, int sRow, int eRow, int **board);
void insertShipH(int row, int sCol, int eCol, int **board);
void print_plyr_board();
//void print_comp_board(); NOT NEEDED ANYMORE
void print_boards();
bool isShip(int sRow, int eRow, int sCol, int eCol, int **board);
void set_board_plyr();
void set_board_comp();
void attack(int row, int col, int **board);
void plyr_turn();
void comp_turn();
void checkShipSunk(int **board);
bool isAllSunk(int **board);
void gameover();
void playgame();

#endif /* battleship_hpp */
