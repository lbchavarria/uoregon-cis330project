//
//  battleship.cpp
//  
//
//  Created by Lucas Chavarria on 2/20/17.
//
//

#include "battleship.hpp"
#include <iostream>
#include <cstdlib>


using namespace std;

const int board_size = 10;
int **plyrboard;
int **compboard;
int comp_ship_count;
int plyr_ship_count;
int comp_row, comp_col, comp_rowO, comp_colO;
int dir;
bool hit_dir = false;
const int water = 0;
const int ship = 1;
const int ship_end = 2;
const int hit = 3;
const int end_hit = 4;
const int checked_hit = 5;
const int miss = 6;
bool is_hit = false;

void initboard(int ***board) {
    (*board) = new int*[board_size];
    for (int i = 0; i < board_size; i++) {
        (*board)[i] = new int[board_size];
        for (int j = 0; j < board_size; j++) {
            (*board)[i][j] = water;
        }
    }
}

void insertShipV(int col, int sRow, int eRow, int **board) {
    for (int i = sRow; i <= eRow; i++) {
        if (i == sRow || i == eRow) {
            board[i][col] = ship_end;
        }
        else {
            board[i][col] = ship;
        }
    }
}

void insertShipH(int row, int sCol, int eCol, int **board) {
    for (int i = sCol; i <= eCol; i++) {
        if (i == sCol || i == eCol) {
            board[row][i] = ship_end;
        }
        else {
            board[row][i] = ship;
        }
    }
}

void print_plyr_board() {
    cout << "  0 1 2 3 4 5 6 7 8 9" << endl;
    for (int i = 0; i < board_size; i++) {
        cout  << i << " ";
        for (int j = 0; j < board_size; j++) {
            if (plyrboard[i][j] == water) {
                cout << ". ";
            }
            else if (plyrboard[i][j] == ship || plyrboard[i][j] == ship_end) {
                cout << "S ";
            }
            else if (plyrboard[i][j] == hit || plyrboard[i][j] == checked_hit || plyrboard[i][j] == end_hit) {
                cout << "H ";
            }
            else if (plyrboard[i][j] == miss) {
                cout << "X ";
            }
        }
        cout << endl;
    }
}

/* NOT NEEDED ANYMORE
void print_comp_board() {
    cout << "  0 1 2 3 4 5 6 7 8 9" << endl;
    for (int i = 0; i < board_size; i++) {
        cout  << i << " ";
        for (int j = 0; j < board_size; j++) {
            if (compboard[i][j] == hit) {
                cout << "H ";
            }
            else if (compboard[i][j] == miss) {
                cout << "X ";
            }
            else {
                cout << ". ";
            }
        }
        cout << endl;
    }

}
*/

void print_boards() {
    cout << "    Player's Board               Computer's Board" << endl;
    cout << "  0 1 2 3 4 5 6 7 8 9          0 1 2 3 4 5 6 7 8 9" << endl;
    for (int i = 0; i < board_size; i ++) {
        cout << i << " ";
        for (int j = 0; j < (2*board_size); j++) {
            if (j == board_size) {
                cout << "       ";
                cout << i << " ";
            }
            if (j < board_size) {
                if (plyrboard[i][j] == water) {
                    cout << ". ";
                }
                else if (plyrboard[i][j] == ship || plyrboard[i][j] == ship_end) {
                    cout << "S ";
                }
                else if (plyrboard[i][j] == hit || plyrboard[i][j] == checked_hit || plyrboard[i][j] == end_hit) {
                    cout << "H ";
                }
                /*
                else if (plyrboard[i][j] == end_hit) {
                    cout << "h ";
                }
                */
                else if (plyrboard[i][j] == miss) {
                    cout << "X ";
                }
            }
            if (j >= board_size) {
                if (compboard[i][j-board_size] == hit || compboard[i][j-board_size] == checked_hit || compboard[i][j-board_size] == end_hit) {
                    cout << "H ";
                }
                /*
                else if (compboard[i][j-board_size] == checked_hit) {
                    cout << "h ";
                }
                */
                else if (compboard[i][j-board_size] == miss) {
                    cout << "X ";
                }
                //TESTING ONLY
                /*
                else if (compboard[i][j-board_size] == ship || compboard[i][j-board_size] == ship_end) {
                    cout << "S ";
                }
                */
                else {
                    cout << ". ";
                }
            }
        }
        cout << endl;
    }
}

bool isShip(int sRow, int eRow, int sCol, int eCol, int **board) {
    for (int i = sRow; i <= eRow; i++) {
        for (int j = sCol; j <= eCol; j++) {
            if (board[i][j] == ship || board[i][j] == ship_end) {
                return true;
            }
        }
    }
    return false;
}

void set_board_plyr() {
    int size;
    int sRow, eRow, sCol, eCol;
    char hv;
    while (plyr_ship_count < 5) {
        print_plyr_board();
        //change size of ship to insert depending on how many ships are on the board
        switch (plyr_ship_count) {
            case 0: size = 5;
                    break;
            case 1: size = 4;
                    break;
            case 2: size = 3;
                    break;
            case 3: size = 3;
                    break;
            case 4: size = 2;
                    break;
        }
        //Obtain coordinates of each end of the ship
        do {
            cout << "Insert ship of size " << size << endl;
            //Beginning coordinates initially left-most or top-most part of ship
            cout << "Insert beginning coordinates" << endl;
            cout << "Row (0-9) and Column (0-9): ";
            cin >> sRow;
            cin >> sCol;
            //cout << "Column: ";
            //cin >> sCol;
            //cout << "Insert ending coordinates" << endl;
            //cout << "Row: ";
            cout << "Horizontal(H) or Vertical(V): ";
            cin >> hv;
            if (toupper(hv) == 'H') {
                eRow = sRow;
                if ((sCol+(size-1)) < board_size) {
                    eCol = sCol + (size-1);
                }
                else {
                    eCol = sCol - (size-1);
                }
            }
            if (toupper(hv) == 'V') {
                eCol = sCol;
                if ((sRow+(size-1)) < board_size) {
                    eRow = sRow + (size-1);
                }
                else {
                    eRow = sRow - (size-1);
                }
            }
            //cin >> eRow;
            //cout << "Col: ";
            //cin >> eCol;
            //Check if ship overlaps another ship
            if (isShip(sRow, eRow, sCol, eCol, plyrboard)) {
                cout << "Ship is overlapping another ship" << endl;
            }
        }while (isShip(sRow, eRow, sCol, eCol, plyrboard));
        if (toupper(hv) == 'V'/*sRow - eRow == (size - 1) || sRow - eRow == ((size * -1) + 1)*/) {
            if (sRow < eRow) {
                insertShipV(sCol, sRow, eRow, plyrboard);
            }
            else if (eRow < sRow) {
                insertShipV(sCol, eRow, sRow, plyrboard);
            }
        }
        else if (toupper(hv) == 'H'/*sCol - eCol == (size - 1) || sCol - eCol == ((size * -1) + 1)*/) {
            if (sCol < eCol) {
                insertShipH(sRow, sCol, eCol, plyrboard);
            }
            else if (eCol < sCol) {
                insertShipH(sRow, eCol, sCol, plyrboard);
            }
        }
        plyr_ship_count++;
        
        
    }

    
}

void set_board_comp() {
    int size, dir;
    int sRow, eRow, sCol, eCol;
    srand(time(NULL));
    while (comp_ship_count < 5) {
        switch (comp_ship_count) {
            case 0: size = 5;
                break;
            case 1: size = 4;
                break;
            case 2: size = 3;
                break;
            case 3: size = 3;
                break;
            case 4: size = 2;
                break;
        }
        do {
            dir = rand() % 2;
            sRow = rand() % 10;
            sCol = rand() % 10;
            if (dir == 0) {
                eRow = sRow + size - 1;
                eCol = sCol;
                if (eRow > (board_size-1)) {
                    eRow = sRow - size + 1;
                }
            }
            else if (dir == 1) {
                eCol = sCol + size - 1;
                eRow = sRow;
                if (eCol > (board_size-1)) {
                    eCol = sCol - size + 1;
                }

            }
        }while(isShip(sRow, eRow, sCol, eCol, compboard) && eCol < board_size && eRow < board_size);
        if (dir == 0) {
            if (sRow < eRow && !isShip(sRow, eRow, sCol, eCol, compboard)) {
                insertShipV(sCol, sRow, eRow, compboard);
                comp_ship_count++;
            }
            else if (eRow < sRow && !isShip(sRow, eRow, sCol, eCol, compboard)) {
                insertShipV(sCol, eRow, sRow, compboard);
                comp_ship_count++;
            }
        }
        else if (dir == 1) {
            if (sCol < eCol && !isShip(sRow, eRow, sCol, eCol, compboard)) {
                insertShipH(sRow, sCol, eCol, compboard);
                comp_ship_count++;
            }
            else if (eCol < sCol && !isShip(sRow, eRow, sCol, eCol, compboard)) {
                insertShipH(sRow, eCol, sCol, compboard);
                comp_ship_count++;
            }
        }
    }
}

void attack(int row, int col, int **board) {
    if (board == plyrboard) {
        cout << "The computer's ";
    }
    if (board == compboard) {
        cout << "Your ";
    }
    if (board[row][col] == ship) {
        board[row][col] = hit;
        cout << "attack hit" << endl;
        checkShipSunk(board);
    }
    else if (board[row][col] == ship_end) {
        board[row][col] = end_hit;
        cout << "attack hit" << endl;
        checkShipSunk(board);
    }
    else if (board[row][col] == water) {
        board[row][col] = miss;
        cout << "attack missed" << endl;
    }
    else {
        cout << "Already fired here. Please try another point to fire at" << endl;
        cout << "Row (0-9) and Column (0-9): ";
        cin >> row;
        //cout << "Column (0-9): ";
        cin >> col;
        attack(row, col, board);
    }
}

void plyr_turn() {
    //print_comp_board();
    //cout << endl;
    //print_plyr_board();
    print_boards();
    int row, col;
    cout << "Input firing coordinates" << endl;
    do {
        cout << "Row (0-9) and Column (0-9): ";
        cin >> row;
        //cout << "Column (0-9): ";
        cin >> col;
        if (row < 0 || row > 9 || col < 0 || col > 9) {
            cout << "Invalid input. Please try another point to fire at" << endl;
        }
    }while (row < 0 || row > 9 || col < 0 || col > 9);
    attack(row, col, compboard);
}

void comp_turn() {
    srand(time(NULL));
    if (is_hit) {
        if (!hit_dir) {
            dir = rand() % 4;
        }
        int missnum = 0;
        //switch (dir) {
        while (true) {
            if (dir == 0) {
                if (comp_row+1 < board_size && (plyrboard[comp_row+1][comp_col] != hit && plyrboard[comp_row+1][comp_col] != miss && plyrboard[comp_row+1][comp_col] != checked_hit && plyrboard[comp_row+1][comp_col] != end_hit)) {
                    attack(comp_row+1, comp_col, plyrboard);
                    if (plyrboard[comp_row+1][comp_col] == hit || plyrboard[comp_row+1][comp_col] == end_hit) {
                        comp_row = comp_row+1;
                        hit_dir = true;
                    }
                    break;
                }
                else if (comp_row+1 >= board_size || plyrboard[comp_row+1][comp_col] == hit || plyrboard[comp_row+1][comp_col] == miss || plyrboard[comp_row+1][comp_col] == checked_hit || plyrboard[comp_row+1][comp_col] == end_hit) {
                    dir++;
                    missnum++;
                    if (missnum == 4) {
                        comp_row = comp_rowO;
                        comp_col = comp_colO;
                    }
                    if (missnum == 8) {
                        is_hit = false;
                        break;
                    }
                }
            }
            if (dir == 1) {
                if (comp_col+1 < board_size && (plyrboard[comp_row][comp_col+1] != hit && plyrboard[comp_row][comp_col+1] != miss && plyrboard[comp_row][comp_col+1] != checked_hit && plyrboard[comp_row][comp_col+1] != end_hit)) {
                    attack(comp_row, comp_col+1, plyrboard);
                    if (plyrboard[comp_row][comp_col+1] == hit || plyrboard[comp_row][comp_col+1] == end_hit) {
                        comp_col = comp_col+1;
                        hit_dir = true;
                    }
                    break;
                }
                else if (comp_col+1 >= board_size || plyrboard[comp_row][comp_col+1] == hit || plyrboard[comp_row][comp_col+1] == miss || plyrboard[comp_row][comp_col+1] == checked_hit || plyrboard[comp_row][comp_col+1] == end_hit) {
                    dir++;
                    missnum++;
                    if (missnum == 4) {
                        comp_row = comp_rowO;
                        comp_col = comp_colO;
                    }
                    if (missnum == 8) {
                        is_hit = false;
                        break;
                    }
                }
            }
            if (dir == 2) {
                if (comp_row-1 >= 0 && (plyrboard[comp_row-1][comp_col] != hit && plyrboard[comp_row-1][comp_col] != miss && plyrboard[comp_row-1][comp_col] != checked_hit && plyrboard[comp_row-1][comp_col] != end_hit)) {
                    attack(comp_row-1, comp_col, plyrboard);
                    if (plyrboard[comp_row-1][comp_col] == hit || plyrboard[comp_row-1][comp_col] == end_hit) {
                        comp_row = comp_row-1;
                        hit_dir = true;
                    }
                    break;
                }
                else if (comp_row-1 < 0 || plyrboard[comp_row-1][comp_col] == hit || plyrboard[comp_row-1][comp_col] == miss || plyrboard[comp_row-1][comp_col] == checked_hit || plyrboard[comp_row-1][comp_col] == end_hit) {
                    dir++;
                    missnum++;
                    if (missnum == 4) {
                        comp_row = comp_rowO;
                        comp_col = comp_colO;
                    }
                    if (missnum == 8) {
                        is_hit = false;
                        break;
                    }
                }
            }
            if (dir == 3) {
                if (comp_col-1 >= 0 && (plyrboard[comp_row][comp_col-1] != hit && plyrboard[comp_row][comp_col-1] != miss && plyrboard[comp_row][comp_col-1] != checked_hit && plyrboard[comp_row][comp_col-1] != end_hit)) {
                    attack(comp_row, comp_col-1, plyrboard);
                    if (plyrboard[comp_row][comp_col-1] == hit || plyrboard[comp_row][comp_col-1] == end_hit) {
                        comp_col = comp_col-1;
                        hit_dir = true;
                    }
                    break;
                }
                else if (comp_col-1 < 0 || plyrboard[comp_row][comp_col-1] == hit || plyrboard[comp_row][comp_col-1] == miss || plyrboard[comp_row][comp_col-1] == checked_hit || plyrboard[comp_row][comp_col-1] == end_hit) {
                    dir = 0;
                    missnum++;
                    if (missnum == 4) {
                        comp_row = comp_rowO;
                        comp_col = comp_colO;
                    }
                    if (missnum == 8) {
                        is_hit = false;
                        break;
                    }
                }
            }
        }
    }
    if (!is_hit) {
        do {
            comp_row = rand() % 10;
            comp_col = rand() % 10;
        }while (plyrboard[comp_row][comp_col] == hit || plyrboard[comp_row][comp_col] == miss || plyrboard[comp_row][comp_col] == checked_hit || plyrboard[comp_row][comp_col] == end_hit);
        attack(comp_row, comp_col, plyrboard);
        if (plyrboard[comp_row][comp_col] == hit || plyrboard[comp_row][comp_col] == end_hit) {
            is_hit = true;
            comp_rowO =comp_row;
            comp_colO = comp_col;
        }
    }
}

/* REPLACING
void checkShipSunk(int **board) {
    int size = 0;
    const int top_size = 5;
    int d = 0;
    for (int i = 0; i < board_size; i++) {
        for (int j = 0; j < board_size; j++) {
            if (board[i][j] == hit) {
                size++;
                for (int k = 1; k <= top_size; k++) {
                    if ((i+k) < board_size || (i-k) >= 0 || (j+k) < board_size || (j-k) >= 0) {
                        if (d == 0) {
                            if (board[i+k][j] == hit) {
                            size++;
                            }
                            else if (board[i+k][j] == ship) {
                                return;
                            }
                            else if (k == 1) {
                                d++;
                            }
                            else {
                                if (board == plyrboard) {
                                cout << "The computer has sunk a ship" << endl;
                                    is_hit = false;
                                }
                                if (board == compboard) {
                                    cout << "You have sunk a ship" << endl;
                                }
                                for (int l = k; l <= 0; l--) {
                                    board[i+k-l][j] = checked_hit;
                                }
                                return;
                            }
                            
                        }
                        if (d == 1) {
                            if (board[i-k][j] == hit) {
                                size++;
                            }
                            else if (board[i-k][j] == ship) {
                                return;
                            }
                            else if (k == 1) {
                                d++;
                            }
                            else {
                                if (board == plyrboard) {
                                    cout << "The computer has sunk a ship" << endl;
                                }
                                if (board == compboard) {
                                    cout << "You have sunk a ship" << endl;
                                }
                                for (int l = k; l <= 0; l--) {
                                    board[i-k+l][j] = checked_hit;
                                }
                                return;
                            }
                        }
                        if (d == 2) {
                            if (board[i][j+k] == hit) {
                                size++;
                            }
                            else if (board[i][j+k] == ship) {
                                return;
                            }
                            else if (k == 1) {
                                d++;
                            }
                            else {
                                if (board == plyrboard) {
                                    cout << "The computer has sunk a ship" << endl;
                                }
                                if (board == compboard) {
                                    cout << "You have sunk a ship" << endl;
                                }
                                for (int l = k; l <= 0; l--) {
                                    board[i][j+k-l] = checked_hit;
                                }
                                return;
                            }
                        }
                        if (d == 3) {
                            if (board[i][j-k] == hit) {
                                size++;
                            }
                            else if (board[i][j-k] == ship) {
                                return;
                            }
                            else {
                                if (board == plyrboard) {
                                    cout << "The computer has sunk a ship" << endl;
                                }
                                if (board == compboard) {
                                    cout << "You have sunk a ship" << endl;
                                }
                                for (int l = k; l <= 0; l--) {
                                    board[i][j-k+l] = checked_hit;
                                }
                                return;
                            }
                        }
                    }
                }
            }
        }
    }
}
*/

void checkShipSunk(int **board) {
    const int top_size = 5;
    int d = 0;
    bool hit_found = false;
    for (int i = 0; i < board_size; i++) {
        for (int j = 0; j < board_size; j++) {
            if (board[i][j] == end_hit) {
               //cout << "found end_hit" << endl;
                for (int k = 1; k < top_size; k++) {
                    if (d == 0 && board[i+k][j] != water) {
                        //cout << "check direction 1" << endl;
                        if (i+k < board_size) {
                            //cout << "inside board" << endl;
                            if (board[i+k][j] == ship) {
                                //cout << "found remaining ship" << endl;
                                hit_found = false;
                                return;
                            }
                            else if (board[i+k][j] == hit) {
                                //cout << "found hit" <<  endl;
                                hit_found = true;
                            }
                            else if (board[i+k][j] == end_hit) {
                                for (int l = k; l >= 0; l--) {
                                    //cout << "check" << endl;
                                    board[i+k-l][j] = checked_hit;
                                }
                                //cout << "ship sunk" << endl;
                                if (board == plyrboard) {
                                    cout << "The computer has sunk a ship" << endl;
                                }
                                else if (board == compboard) {
                                    cout << "You have sunk a ship" << endl;
                                }
                                
                                return;
                            }
                        }
                    }
                    if (k == 1 && !hit_found) {
                        //cout << "wrong direction" << endl;
                        d++;
                    }
                    /*if (d == 1) {
                        if (i-k < board_size) {
                            if (board[i-k][j] == ship) {
                                hit_found = false;
                                return;
                            }
                            else if (board[i-k][j] == hit) {
                                hit_found = true;
                            }
                            else if (board[i-k][j] == end_hit) {
                                for (int l = k; l >= 0; l--) {
                                    board[i-k+l][j] = checked_hit;
                                }
                                if (board == plyrboard) {
                                    cout << "The computer has sunk a ship" << endl;
                                }
                                else if (board == compboard) {
                                    cout << "You have sunk a ship" << endl;
                                }
                                
                                return;
                            }
                        }
                        else if (k == 1 && !hit_found) {
                            d++;
                        }
                    }*/
                    if (d == 1) {
                        if (j+k < board_size) {
                            //cout << "yes" << endl;
                            if (board[i][j+k] == ship) {
                                hit_found = false;
                                //cout << "ship" << endl;
                                return;
                            }
                            else if (board[i][j+k] == hit) {
                                hit_found = true;
                                //cout << "hit" << endl;
                            }
                            else if (board[i][j+k] == end_hit) {
                                //cout << "sunk" << endl;
                                for (int l = k; l >= 0; l--) {
                                    board[i][j+k-l] = checked_hit;
                                }
                                if (board == plyrboard) {
                                    cout << "The computer has sunk a ship" << endl;
                                }
                                else if (board == compboard) {
                                    cout << "You have sunk a ship" << endl;
                                }
                                
                                return;
                            }
                        }
                        /*else if (k == 1 && !hit_found) {
                            d++;
                        }*/
                    }
                    /*if (d == 3) {
                        if (j-k < board_size) {
                            if (board[i][j-k] == ship) {
                                hit_found = false;
                                return;
                            }
                            else if (board[i][j-k] == hit) {
                                hit_found = true;
                            }
                            else if (board[i][j-k] == end_hit) {
                                for (int l = k; l >= 0; l--) {
                                    board[i][j-k+l] = checked_hit;
                                }
                                if (board == plyrboard) {
                                    cout << "The computer has sunk a ship" << endl;
                                }
                                else if (board == compboard) {
                                    cout << "You have sunk a ship" << endl;
                                }
                                
                                return;
                            }
                        }
                    }*/
                }
            }
        }
    }
}

bool isAllSunk(int **board) {
    for (int i = 0; i < board_size; i++) {
        for (int j = 0; j < board_size; j++) {
            if (board[i][j] == ship || board[i][j] == ship_end) {
                return false;
            }
        }
    }
    return true;
}

void gameover() {
    if (isAllSunk(compboard)) {
        cout << "You Win" << endl;
    }
    else if (isAllSunk(plyrboard)) {
        cout << "You Lose" << endl;
    }
    for (int i = 0; i < board_size; i++) {
        delete[] plyrboard[i];
        delete[] compboard[i];
    }
    delete[] plyrboard;
    delete[] compboard;
}

void playgame() {
    
    initboard(&plyrboard);
    initboard(&compboard);
    
    set_board_plyr();
    cout << "Player's board set" << endl;
    set_board_comp();
    
    while(true) {
        plyr_turn();
        if (isAllSunk(compboard)) {
            break;
        }
        comp_turn();
        if (isAllSunk(plyrboard)) {
            break;
        }
    }
    
    gameover();
}
