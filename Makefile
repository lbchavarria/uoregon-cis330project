all: battleship

battleship: main.cpp battleship.cpp
	g++ -std=c++11 main.cpp battleship.cpp -o battleship

clean:
	rm battleship
